// Where will the craftmaster configuration be found (relative to the working directory that is)?
def craftmasterConfigDir = "binary-factory-tooling/craft/configs/prebuilt/"

// Request a node to be allocated to us
node( "WindowsMSVC" ) {
// We want Timestamps on everything
timestamps {
	// We want to catch any errors that occur to allow us to send out notifications (ie. emails) if needed
	catchError {

		// First things first, make sure everything that could be using up disk space is removed
		// This ensures that WebEngine builds don't get tripped up...
		stage('Cleaning Up Craft') {
			bat """
				rmdir /s /q C:\\Craft\\BC\\
				rmdir /s /q C:\\Craft\\BinaryFactory\\
				rmdir /s /q C:\\Craft\\Downloads\\
				rmdir /s /q C:\\_\\
				exit /b 0
			"""
		}

		// First we want to make sure Craft is ready to go
		stage('Preparing Craft') {
			// Make sure we start with a clean slate
			deleteDir()

			// Grab our tooling which we will need in a few moments
			checkout changelog: false, poll: false, scm: [
				$class: 'GitSCM',
				branches: [[name: 'master']],
				extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'bf-tooling/']],
				userRemoteConfigs: [[url: 'https://anongit.kde.org/sysadmin/binary-factory-tooling']]
			]

			// We are also reliant on the CI System Tooling for parts of this, so grab it too
			checkout changelog: false, poll: false, scm: [
				$class: 'GitSCM',
				branches: [[name: 'master']],
				extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'ci-tooling/']],
				userRemoteConfigs: [[url: 'https://anongit.kde.org/sysadmin/ci-tooling']]
			]

			// Make sure that Craftmaster is ready to go
			bat """
				python "%WORKSPACE%\\bf-tooling\\craft\\checkout-repository.py" --repository "git://anongit.kde.org/craftmaster" --into "C:/Craft/BC/"
				python "%WORKSPACE%\\bf-tooling\\craft\\checkout-repository.py" --repository "git://anongit.kde.org/sysadmin/binary-factory-tooling" --into "C:/Craft/BC/"
			"""
		}

		// Now that is done, it's time to rebuild the Craft Cache!
		stage('Build Craft Cache') {
			bat """
				setlocal enabledelayedexpansion

				cd C:/Craft/BC/

				python craftmaster/Craftmaster.py --config ${craftmasterConfigDir}/CraftBinaryCache.ini --target ${craftPlatform} --variables CreateCache=True UseCache=True -c -i craft
				python craftmaster/Craftmaster.py --config ${craftmasterConfigDir}/CraftBinaryCache.ini --target ${craftPlatform} --variables CreateCache=True UseCache=True -c --add-blueprint-repository https://invent.kde.org/vonreth/craft-blueprints-prebuilt.git
				if !errorlevel! neq 0 exit /b !errorlevel!
				python craftmaster/Craftmaster.py --config ${craftmasterConfigDir}/CraftBinaryCache.ini --target ${craftPlatform} --variables CreateCache=True UseCache=True -c --list-file ${craftmasterConfigDir}/BinaryCachePackages.list
				if !errorlevel! neq 0 exit /b !errorlevel!
				if "${craftPlatform}" == "windows-mingw_64-gcc" GOTO autotools
				if "${craftPlatform}" == "windows-mingw_32-gcc" GOTO autotools
				GOTO end
				:autotools
					python craftmaster/Craftmaster.py --config ${craftmasterConfigDir}/CraftBinaryCache.ini --target ${craftPlatform} --variables CreateCache=True UseCache=True -c --list-file ${craftmasterConfigDir}/autotools.list
					if !errorlevel! neq 0 exit /b !errorlevel!
					python craftmaster/Craftmaster.py --config ${craftmasterConfigDir}/CraftBinaryCache.ini --target ${craftPlatform} --variables CreateCache=True UseCache=True -c --list-file ${craftmasterConfigDir}/autotools_package.list --package --option [Packager]PackageType=PortablePackager
				:end
				if !errorlevel! neq 0 exit /b !errorlevel!
			"""
		}
	}

	// We need to catch errors here to ensure that the cleanup always runs, even if the upload fails
	// While this is a bit wasteful it at least determines that errors in the publishing process leave a builder with a very full disk!
	catchError {
		// Upload it to the master server
		stage('Publish Craft Cache') {
			bat """
				cd "%WORKSPACE%\\ci-tooling"
				python helpers/sftp-directory-mirror.py --source C:/Craft/BC/cache/ --destination /srv/archives/files/craft/prebuilt/ --server milonia.kde.org --username craft
			"""
		}
	}

	// Then we can cleanup everything we did
	// It is critical this always happens, otherwise builders can run out of disk space!
	stage('Cleaning Up') {
		bat """
			rmdir /s /q C:\\Craft\\BC\\
		"""
	}

	// As the Windows Slaves are permanent ones, we erase the Workspace as the last thing we do
	deleteDir()
}
}
