// Where will the craftmaster configuration be found (relative to the working directory that is)?
def craftmasterConfigDir = "binary-factory-tooling/craft/configs/experimental/"

// Request a node to be allocated to us
node( "Ubuntu1604Craft" ) {
// We want Timestamps on everything
timestamps {
	// We want to catch any errors that occur to allow us to send out notifications (ie. emails) if needed
	catchError {

		// First we want to make sure Craft is ready to go
		stage('Preparing Craft') {
			// Grab our tooling which we will need in a few moments
			checkout changelog: false, poll: false, scm: [
				$class: 'GitSCM',
				branches: [[name: 'master']],
				extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'bf-tooling/']],
				userRemoteConfigs: [[url: 'https://anongit.kde.org/sysadmin/binary-factory-tooling']]
			]

			// We are also reliant on the CI System Tooling for parts of this, so grab it too
			checkout changelog: false, poll: false, scm: [
				$class: 'GitSCM',
				branches: [[name: 'master']],
				extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'ci-tooling/']],
				userRemoteConfigs: [[url: 'https://anongit.kde.org/sysadmin/ci-tooling']]
			]

			// Make sure that Craftmaster is ready to go
			sh """
				python3 "$WORKSPACE/bf-tooling/craft/checkout-repository.py" --repository "git://anongit.kde.org/craftmaster" --into ~/Craft/BC/
				python3 "$WORKSPACE/bf-tooling/craft/checkout-repository.py" --repository "git://anongit.kde.org/sysadmin/binary-factory-tooling" --into ~/Craft/BC/
			"""
		}

		// Now that is done, it's time to rebuild the Craft Cache!
		stage('Build Craft Cache') {
			sh """
				cd ~/Craft/BC/
				python3.6 craftmaster/CraftMaster.py --config ${craftmasterConfigDir}/CraftBinaryCache.ini --target ${craftPlatform} --variables CreateCache=True UseCache=True -c -i craft
				python3.6 craftmaster/CraftMaster.py --config ${craftmasterConfigDir}/CraftBinaryCache.ini --target ${craftPlatform} --variables CreateCache=True UseCache=True -c --option "/.buildTests=False" --list-file ${craftmasterConfigDir}/BinaryCachePackagesBase.list
				python3.6 craftmaster/CraftMaster.py --config ${craftmasterConfigDir}/CraftBinaryCache.ini --target ${craftPlatform} --variables CreateCache=True UseCache=True -c --option "/.buildTests=False" --list-file ${craftmasterConfigDir}/BinaryCachePackages.list
			"""
		}

	}

    // And finally, we'll upload it to the master server
    stage('Publish Craft Cache') {
        sh """
            cd "$WORKSPACE/ci-tooling/"
            python3 helpers/sftp-directory-mirror.py --source ~/Craft/BC/cache/ --destination /srv/archives/files/craft/master/ --server milonia.kde.org --username craft
        """
    }
}
}
