// Request a node to be allocated to us
node( "WindowsMingw" ) {
// We want Timestamps on everything
timestamps {
	// We want to catch any errors that occur to allow us to send out notifications (ie. emails) if needed
	catchError {

		// First Thing: Checkout Sources
		stage('Checkout Sources') {
			// Make sure we have a clean slate to begin with
			deleteDir()

			// Krita Code
			checkout changelog: true, poll: true, scm: [
				$class: 'GitSCM',
				branches: [[name: 'master']],
				extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'krita/']],
				userRemoteConfigs: [[url: 'https://anongit.kde.org/krita']]
			]

		}

		// Now we can build the dependencies of Krita
		stage('Building Dependencies') {
			// Before we can do this we need to setup some environment variables to help guide the Krita scripts around
			// In particular it needs:
			// PATH set to guide it to CMake and to allow it to find Mingw (Git and Python are already in PATH as standard convention for these builders)
			// SEVENZIP_EXE set to guide it to 7-Zip's console utility. It's detection support doesn't work as our binary is named 7za.exe instead of the apparently standard 7z.exe
			// Then we can run it
			bat """
				set PATH=C:/MingW/i686-7.3.0-posix-dwarf-rt_v5-rev0/mingw32/bin;C:/PROGRA~2/Python38-32/;C:/Craft/Krita/windows-msvc2019_64-cl/bin;C:/Craft/Krita/windows-msvc2019_64-cl/dev-utils/bin;%PATH%
				set SEVENZIP_EXE=C:/Craft/Krita/windows-msvc2019_64-cl/dev-utils/bin/7za.exe
				set GETTEXT_SEARCH_PATH=C:/Craft/Krita/windows-msvc2019_64-cl/bin/;C:/PROGRA~2/Python38-32/

				mkdir "C:\\Packaging\\KritaWS\\"
				mkdir "C:\\Packaging\\KritaDepsBuild\\"

				cmd /c krita\\build-tools\\windows\\build.cmd --no-interactive --skip-krita --src-dir "%WORKSPACE%\\krita" --download-dir "%WORKSPACE%\\downloads" --deps-build-dir "C:\\Packaging\\KritaDepsBuild\\" --deps-install-dir "C:\\Packaging\\KritaWS\\deps-install"

				if errorlevel 1 exit /b %errorlevel%
			"""
		}

		// Now we capture them for use 
		stage('Capturing Dependencies') {
			// First we zip all of the dependencies up...
			bat """
			    cd C:/Packaging/KritaWS/
				C:/Craft/Krita/windows-msvc2019_64-cl/dev-utils/bin/7za.exe a "%WORKSPACE%/krita-deps.zip" deps-install/
			"""
			
			// Then we ask Jenkins to capture the zip file as an artifact
			archiveArtifacts artifacts: 'krita-deps.zip', onlyIfSuccessful: true
		}
	}

	// As the Windows Slaves are permanent ones, we erase the Workspace as the last thing we do
	deleteDir()

	// Same goes for the stable workspace that we use
	bat """
	   rmdir /s /q "C:\\Packaging\\KritaWS\\"
	   rmdir /s /q "C:\\Packaging\\KritaDepsBuild\\"
	"""
}
}
