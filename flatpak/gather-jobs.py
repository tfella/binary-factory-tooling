#!/usr/bin/python3
import os
import sys
import json
import yaml
import argparse

# Parse the command line arguments we've been given
parser = argparse.ArgumentParser(description='Utility to determine which jobs need to be registered in Jenkins.')
parser.add_argument('--flatpak-manifests', type=str, required=True)
arguments = parser.parse_args()

# Our output will be a list of Dictionaries, containing several keys:
# 1) The name of the job
# 2) The Craft Blueprints to be built as part of the job
# 3) The Craft Blueprints the job should package
# 4) The description for the resulting job
jobsGathered = []

def findProjectSource(manifest):
    for module in reversed(manifest['modules']):
        if not 'sources' in module:
            continue

        for source in reversed(module['sources']):
            if source['type'] == 'git' and 'url' in source:
                return source
    return None

# Let's get started!
for entry in os.scandir(path=arguments.flatpak_manifests):
	# Make sure this isn't a directory (as that definitely isn't a Flatpak definition)
	if entry.is_dir():
		continue

	# Could this be a local Flatpak manifest?
	if entry.name.endswith('.json'):
		# We have a winner!
		# Load the definition in so we can read the information we need from it
		manifestFile = open( entry.path )
		manifest = json.load( manifestFile )

		# Make sure we are dealing with an actual application
		# These have an 'id' specified in them
		if 'id' not in manifest:
			print("Skipping non-application " + entry.name)
			continue

		# We also have to make sure we have a Manifest that specifies a Git repository
		# Otherwise it's impossible to have a nightly build
		source = findProjectSource(manifest)
		if source is None:
			print("Skipping " + entry.name)
			continue

		# Make sure we have a branch for Git
		# This is optional normally but the rest of our code requires it
		if 'branch' not in manifest:
			# Make it master then
			manifest['branch'] = 'master'

		# Grab the repository URL
		# We assume the last repository in the definition is the one we want as this seems to be the case 99% of the time
		# It isn't guaranteed to be correct but it's the best we have due to how Flatpak specifications work
		repositoryUrl = source['url']

		# Determine the path to where the Manifest (.json) file will be when running the build
		manifestPath = "flatpak-kde-applications/{0}".format( entry.name )

		# Grab the ID and grab the last component (assuming that is the application name)
		# Transform any dashes into underscores, as Jenkins does not support dashes in job names
		jobName = "_".join(manifest['id'].split('.')[2:])
		jobName = jobName.replace('-', '_').capitalize()

		# Generate a description...
		jobDescription = "{0} nightly build for Flatpak".format( manifest['id'] )

		# Add it to the list of jobs to be built - first we do AMD64
		jobsGathered.append( {
			'name': jobName,
			'description': jobDescription,
			'manifest': manifestPath,
			'repositoryUrl': repositoryUrl,
			'branch': manifest['branch'],
			'script': 'flatpak/generic-amd64-build.pipeline'
		})

		# Then we do ARM
		jobsGathered.append( {
			'name': jobName + "_arm",
			'description': jobDescription,
			'manifest': manifestPath,
			'repositoryUrl': repositoryUrl,
			'branch': manifest['branch'],
			'script': 'flatpak/generic-arm-build.pipeline'
		})

	# Otherwise maybe it could be a remote Flatpak manifest?
	if entry.name.endswith('remoteapp'):
		# Another winner!
		# Because this file is shell format we'll need to do some work to make it usable
		manifestFile = open( entry.path )
		manifest = {}

		# Go over each line in turn
		for line in manifestFile.readlines():
			# Lines are formatted following the KEY=VALUE syntax
			key, value = line.strip().split('=', 1)
			# Add it to the Manifest we're creating
			manifest[key] = value

		# Make sure we have a branch for Git
		# This is optional normally but the rest of our code requires it
		if 'GITBRANCH' not in manifest:
			# Make it master then
			manifest['GITBRANCH'] = 'master'

		# Grab the ID and grab the last component (assuming that is the application name)
		# Transform any dashes into underscores, as Jenkins does not support dashes in job names
		jobName = manifest['ID'].split('.')[-1]
		jobName = jobName.replace('-', '_').capitalize()

		# Generate a description...
		jobDescription = "{0} nightly build for Flatpak".format( manifest['ID'] )

		# Add it to the list of jobs to be built - first we do AMD64...
		jobEntry = {
			'name': jobName,
			'description': jobDescription,
			'manifest': manifest['JSON'],
			'repositoryUrl': manifest['GITURL'],
			'branch': manifest['GITBRANCH'],
			'script': 'flatpak/generic-amd64-build.pipeline'
		}

		# Then we do ARM
		jobsGathered.append( {
			'name': jobName + "_arm",
			'description': jobDescription,
			'manifest': manifest['JSON'],
			'repositoryUrl': manifest['GITURL'],
			'branch': manifest['GITBRANCH'],
			'script': 'flatpak/generic-arm-build.pipeline'
		})

		jobsGathered.append( jobEntry )

	# If it isn't any of those two then we don't care about it - continue onward to the next one!
	continue

# Now output the jobs we've gathered in JSON to disk
# This will subsequently be read in by a Jenkins DSL script and turned into Jenkins Jobs
filePath = os.path.join( os.getcwd(), 'gathered-jobs.json')
with open(filePath, 'w') as jobsFile:
	json.dump( jobsGathered, jobsFile, sort_keys=True, indent=2  )

# All done!
sys.exit(0)
