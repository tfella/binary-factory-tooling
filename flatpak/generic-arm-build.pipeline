// Request a node to be allocated to us
node( "FlatpakARM" ) {
// We want Timestamps on everything
timestamps {
	// We want to catch any errors that occur to allow us to send out notifications (ie. emails) if needed
	catchError {
		// Make sure the local environment is cleaned up
		stage('Cleaning up') {
			// We don't want to use a previously used staging repository, to make sure it doesn't exist
			sh """
				rm -rf ~/staging-repo/
			"""
		}

		// Retrieve the build instructions for Flatpak
		stage('Fetching Sources') {

			// Actual Application Sources
			checkout changelog: true, poll: true, scm: [
				$class: 'GitSCM',
				branches: [[name: branchToBuild]],
				extensions: [[$class: 'CloneOption', timeout: 120]],
				userRemoteConfigs: [[url: repositoryUrl]]
			]

			// General Flatpak build recipe repository
			checkout changelog: false, poll: false, scm: [
				$class: 'GitSCM',
				branches: [[name: 'master']],
				extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'flatpak-kde-applications/']],
				userRemoteConfigs: [[url: 'https://anongit.kde.org/flatpak-kde-applications']]
			]

		}

		// Build the application!
		stage('Building Application') {
			// Build it!
			// We conduct all builds from ~/jenkins to allow for the compiler cache to be shared between builds
			// It isn't necessary to create ~/staging-repo in advance as flatpak-builder will do that for us
			// We remove the app/ folder afterwards to be clean and tidy as we won't be inspecting it manually - we only want the artifacts in the repository!
			// Further, because we may be on a fresh builder, we make sure we add the necessary remotes for the build as part of our preparation process
			sh """
				cd ~/jenkins/

				flatpak --user remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

				BUILD_DATE=`date`
				flatpak-builder --force-clean --delete-build-dirs --arch=arm --ccache --sandbox --user --install-deps-from=flathub --repo=\$HOME/staging-repo/ --subject="Built on \$BUILD_DATE" app "\$WORKSPACE/${manifest}"

				rm -rf app/
			"""
		}

		// Tarball the repository up and capture it for transfer to the signer
		stage('Exporting Build') {
			// Create the tarball - we'll skip generating static delates as the staging repository doesn't need them
			sh """
				cd \$HOME
				tar -cf \$WORKSPACE/staging-repo.tar staging-repo/
			"""

			// Capture the tarball....
			stash includes: 'staging-repo.tar', name: 'staging-repo'
		}

		// Cleanup on the way out
		deleteDir()
	}
}
}

// Transition to the Flatpak repository signer
node( "FlatpakSigner" ) {
// We want Timestamps on everything
timestamps {
	// We want to catch any errors that occur to allow us to send out notifications (ie. emails) if needed
	catchError {
		// Now that we are on the secure, enduring, signing machine we can begin the process of publishing and signing our build!
		stage('Publishing Repository') {
			// Make sure we have a clean environment first
			deleteDir()

			// Retrieve the tarball from the earlier build
			unstash 'staging-repo'

			// Now we can integrate it, sign it and publish it!
			sh """
				tar -xf staging-repo.tar

				flatpak build-commit-from --src-repo=staging-repo --gpg-sign=61C45BED \$HOME/production-repo --update-appstream
				flatpak build-update-repo --generate-static-deltas --gpg-sign=61C45BED \$HOME/production-repo

				rsync -Ha --delete \$HOME/production-repo/ flatpak@milonia.kde.org:/srv/www/distribute.kde.org/flatpak-apps-testing/
			"""

			// Ensure we don't leave a mess behind either
			deleteDir()
		}
	}
}
}
